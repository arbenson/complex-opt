!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! File minresqlpDataModule.f90
!
! Defines precision and range in real(kind=dp) and integer(kind=ip) for
! portability and a few constants for use in other modules.
!
! 14 Oct 2007: First version implemented after realizing -r8 is not
!              a standard compiler option.
! 15 Oct 2007: Temporarily used real(8) everywhere.
! 16 Oct 2007: Found that we need
!                 use minresqlpDataModule
!              at the beginning of modules AND inside interfaces.
! 20 Aug 2012: Added single and quadruple real and integer kind 'ip'.
!              Added smallest real positive 'realmin'.
!              Added logical variables for turning on/off aditional tests for
!              SYMMORTHO and MINRESQLP.
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

module minresqlpDataModule

  implicit none

  intrinsic                   ::      selected_real_kind, selected_int_kind, tiny

  ! The following reals are provided for portability. Do not use 'DOUBLE PRECISION'.
  integer,  parameter, public :: dp    = selected_real_kind(15,307)    ! 64-bit real, default
  integer,  parameter, public :: sp    = selected_real_kind(6,37)      ! 32-bit real
 !integer,  parameter, public :: qp    = selected_real_kind(33,4931)   !128-bit real

  integer,  parameter, public :: ip   =  selected_int_kind(9)         ! R: (-10^R, 10^R)

  real(dp), parameter, public :: zero  = 0.0_dp, one =  1.0_dp, eps = epsilon(zero)
  real(dp), parameter, public :: realmin = tiny(one)
  logical,             public :: debug = .false., testMtx = .true., testSymortho = .true.

end module minresqlpDataModule
