!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! File minresqlpTestModule.f90
!
! This file illustrates how MINRESQLP can call Aprod or Msolve
! with a short fixed parameter list, even if it needs arbitrary other data.
!
! 11 Oct 2007: First version of minresqlpTestModule.f90.
! 16 Oct 2007: Use minresqlpDataModule to define dp = selected_real_kind(15).
! 12 Jul 2011: Created complex version zminresqlpTestModule.f90
!              from real version minresqlpTestModule.f90.
! 20 Aug 2012: Added Aprodmtx for multiplying a sparse matrix in
!              Matrix Market Format with a vector.
!              Also added two subroutines minresqlpmtxtest and symorthotest
!              for testing SYMORTHO and MINRESQLP on singular matrices
!              from the Matrix Market.
!              (Aprodmtx is now AprodMtxCPS, AprodMtxCRS, AprodMtxCDS.)
!              (minresqlpmtxtest is now
!               minresqlptestMtxCPS, minresqlptestMtxCRS, minresqlptestMtxCDS.)
! 08 Sep 2012: We are testing "CRS" matrices from Matrix Market
!              (representation type = coordinate, real, symmetric).
!              Found that the MM input routines for "real" type
!              store A in (indx, jndx, rval), not (indx, jndx, dval).
!              Simplified Aprodmtx now works.
! 29 Oct 2012: xcheck in minresqlpCheckModule now used to check x from MINRESQLP.
! 05 Jan 2013: Use minresqlpReadMtxModule for both real and complex MM mtx files.
!              Matrix Market arrays are now allocatable.
!              nnz is determined from the data file.
!              We don't have to guess nnzmax at compile time.
!              AprodMtxCRS replaces Aprodmtx (just the name).
!              AprodMtxCDS added to allow for MM matrices in CDS format (double).
! 21 Apr 2013: AprodMtxCPS added to allow for MM matrices in CPS format
!              (coordinate pattern symmetric, meaning nonzero Aij = 1.0).
!              minresqlptestMtxCRS replaces local subroutine minresqlpmtxtest.
!              minresqlptestMtxCDS, minresqlptestMtxCPS added.
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

module minresqlpTestModule

  use  minresqlpDataModule,    only : dp, sp, ip, eps, zero, one, realmin, debug
  use  minresqlpModule,        only : MINRESQLP, SYMORTHO
  use  minresqlpBlasModule,    only : dnrm2
  use  minresqlpReadMtxModule, only : ReadMtxSize, ReadMtx, nnzmax
  use  minresqlpCheckModule,   only : xcheck

  implicit none

  private
  public   :: minresqlptest, &
              minresqlptestMtxCDS,  minresqlptestMtxCPS,  minresqlptestMtxCRS, & 
              symorthotest
  private  :: Aprod, Msolve, &
              AprodMtxCDS, AprodMtxCPS, AprodMtxCRS

  ! DYNAMIC WORKSPACE DEFINED HERE.
  ! It is allocated in minresqlptest* and used by Aprod* or Msolve.

  real(dp),    allocatable :: d(:)     ! Defines diagonal matrix D.
  real(dp)                 :: Ashift   ! Shift diagonal elements of D in  Msolve.
  real(dp)                 :: Mpert    ! Perturbation to D in Msolve
                                       ! to avoid having an exact preconditioner.

  integer(ip)              :: nnz      ! These ones are used by the
  integer(ip), allocatable :: indx(:)  ! Matrix Market Aprod routines
  integer(ip), allocatable :: jndx(:)  ! AprodMtxCDS, AprodMtxCPS, AprodMtxCRS
  real(dp)   , allocatable :: dval(:)  !
  integer(ip), allocatable :: ival(:)  !
  real(sp)   , allocatable :: rval(:)  !
  complex(dp), allocatable :: cval(:)  !

contains

  !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine Aprod (n,x,y)

    integer(ip), intent(in)  :: n
    real(dp),    intent(in)  :: x(n)
    real(dp),    intent(out) :: y(n)

    !-------------------------------------------------------------------
    ! Aprod  computes y = A*x for some matrix A.
    ! This is a simple example for testing MINRESQLP.
    !-------------------------------------------------------------------

    integer(ip) :: i

    do i = 1, n
       y(i) = d(i)*x(i)
    end do

  end subroutine Aprod

  !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine Msolve(n,x,y)

    integer(ip), intent(in)  :: n
    real(dp),    intent(in)  :: x(n)
    real(dp),    intent(out) :: y(n)

    !-------------------------------------------------------------------
    ! Msolve solves M*y = x for some symmetric positive-definite matrix M.
    ! This is a simple example for testing MINRESQLP.
    ! Ashift will be the same as shift in MINRESQLP.
    !
    ! If Mpert = 0, the preconditioner will be exact, so
    ! MINRESQLP should require either one or two iterations,
    ! depending on whether (A - shift*I) is positive definite or not.
    !
    ! If Mpert is nonzero, somewhat more iterations will be required.
    !-------------------------------------------------------------------

    intrinsic   :: abs, mod
    integer(ip) :: i
    real(dp)    :: di

    do i = 1, n
       di   = abs( d(i) - Ashift )
       if (debug) write(*,*) "i = ", i, ", di = ", di, ", di = ", d(i), ", Ashift = ", Ashift
       if (mod(i,10) == 0) di = di + Mpert
       if (abs(di) > eps) then
          y(i) = x(i) / di
       else
          y(i) = x(i)
       end if
    end do

  end subroutine Msolve

  !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine AprodMtxCDS (n,x,y)

    integer(ip), intent(in)  :: n
    real(dp),    intent(in)  :: x(n)
    real(dp),    intent(out) :: y(n)

    !-------------------------------------------------------------------
    ! AprodMtxCDS  computes y = A*x for some symmetric matrix A
    ! stored in Matrix Market CDS format (coordinate double symmetric).
    ! Only subdiagonal and diagonal elements are in (indx, jndx, dval).
    !-------------------------------------------------------------------

    integer(ip)  :: i, j, k
    real(dp)     :: d

    y(1:n) = zero

    do k = 1, nnz
       i = indx(k)
       j = jndx(k)
       d = dval(k)
!      d = rval(k)

       if (i > j) then          ! d = subdiagonal
          y(i) = y(i) + d*x(j)
          y(j) = y(j) + d*x(i)
       else                     ! i = j, d = diagonal
          y(i) = y(i) + d*x(i)
       end if

!      if (k <= 10) write(*,*) '  ', i, ' ', j, ' ', d,  ' ', y(i)
    end do

  end subroutine AprodMtxCDS

  !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine AprodMtxCPS (n,x,y)

    integer(ip), intent(in)  :: n
    real(dp),    intent(in)  :: x(n)
    real(dp),    intent(out) :: y(n)

    !-------------------------------------------------------------------
    ! AprodMtxCPS  computes y = A*x for some symmetric matrix A
    ! stored in Matrix Market CPS format (coordinate pattern symmetric).
    ! Only subdiagonal and diagonal elements are in (indx, jndx).
    ! The matrix entries are assumed to be 1.0.
    ! 12 Apr 2013: AprodMtxCPS derived from AprodMtxCRS.
    !-------------------------------------------------------------------

    integer(ip)  :: i, j, k

    y(1:n) = zero

    do k = 1, nnz
       i = indx(k)
       j = jndx(k)

       if (i > j) then          ! subdiagonal
          y(i) = y(i) + x(j)
          y(j) = y(j) + x(i)
       else                     ! i = j, diagonal
          y(i) = y(i) + x(i)
       end if

       ! if (k <= 10) write(*,*) '  ', i, ' ', j, ' ', y(i)
    end do

  end subroutine AprodMtxCPS

  !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine AprodMtxCRS (n,x,y)

    integer(ip), intent(in)  :: n
    real(dp),    intent(in)  :: x(n)
    real(dp),    intent(out) :: y(n)

    !-------------------------------------------------------------------
    ! AprodMtxCRS  computes y = A*x for some symmetric matrix A
    ! stored in Matrix Market CRS format (coordinate real symmetric).
    ! Only subdiagonal and diagonal elements are in (indx, jndx, rval).
    !-------------------------------------------------------------------

    integer(ip)  :: i, j, k
    real(dp)     :: d

    y(1:n) = zero

    do k = 1, nnz
       i = indx(k)
       j = jndx(k)
!      d = dval(k)
       d = rval(k)

       if (i > j) then          ! d = subdiagonal
          y(i) = y(i) + d*x(j)
          y(j) = y(j) + d*x(i)
       else                     ! i = j, d = diagonal
          y(i) = y(i) + d*x(i)
       end if

!      if (k <= 10) write(*,*) '  ', i, ' ', j, ' ', d,  ' ', y(i)
    end do

  end subroutine AprodMtxCRS

  !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine minresqlptest( n, precon, shift, pertM, sing, consis, default, nout )

    integer(ip), intent(in)    :: n, nout
    logical,     intent(in)    :: precon, sing, consis
    real(dp),    intent(in)    :: shift, pertM
    logical,     intent(in), optional :: default

    !-------------------------------------------------------------------
    ! minresqlptest solves sets up and solves a system (A - shift*I)x = b,
    ! using Aprod to define A and Msolve to define a preconditioner.
    !-------------------------------------------------------------------

    intrinsic :: real, present

    ! Local arrays and variables
    real(dp)    :: b(n), r1(n), w(n), x(n), xtrue(n), y(n)
    logical     :: checkA, disable, use_default
    integer(ip) :: j, itnlim, istop, itn, nprint
    real(dp)    :: Anorm, Acond, Arnorm, rnorm, rtol, r1norm, xnorm
    real(dp)    :: enorm, etol, wnorm, xnormtrue
    real(dp)    :: maxxnorm, TranCond, Acondlim

    character(len=*), parameter ::  headerStr =                                  &
       "(// '-----------------------------------------------------'"    //       &
       "  / 'Test of  MINRESQLP.'"                                      //       &
       "  / '-----------------------------------------------------'"    //       &
       "  / 'shift  =', f12.4, 6x, 'pertM  =', f12.4)"
    character(len=*), parameter ::  footerStr1 =                                 &
       "(/ '  minresqlp appears to be successful.  n =', i7, '  Itns =', i7," // &
       "'  Relative error in x =', 1p, e8.1)"
    character(len=*), parameter ::  footerStr2 =                                 &
       "(/ '  minresqlp appears to have failed.    n =', i7, '  Itns =', i7," // &
       "'  Relative error in x =', 1p, e8.1)"
    character(len=*), parameter ::  debugStr1 =                                  &
       "(/ 'Final residual =', 1p, e8.1)"
    character(len=*), parameter ::  debugStr2 =                                  &
       "(/ 'Solution  x', 1p, 4(i6, e14.6))"

    write(nout, headerStr) shift, pertM

    allocate( d(n) )           ! Array used in Aprod and Msolve to define A and M.
    Ashift = shift
    Mpert  = pertM

    if (.not. sing) then
      do j = 1, n                     ! Set d(*) to define A.
         d(j) = real(j,dp)/real(n,dp) ! We don't want exact integers.
         xtrue(j) = real(n+1-j,dp)    ! Set the true solution and the rhs
      end do                          ! so that (A - shift*I)*xtrue = b.
    else
      do j = 1, n-2                   ! Set d(*) to define A.
         d(j) = real(j,dp)/real(n,dp) ! We don't want exact integers.
         xtrue(j) = real(n+1-j,dp)    ! Set the true solution and the rhs
      end do                          ! so that (A - shift*I)*xtrue = b.
      d(n-1:n) = zero
      xtrue(n-1:n) = zero
    end if

    call Aprod (n,xtrue,b)   ! b = A*xtrue
    b      = b - shift*xtrue ! Now b = (A - shift*I)*xtrue
    if (.not. consis) then
       b(n-1:n) = 1_dp       ! b not in the range of A
    end if

    !debug = .false.
    if (debug) then
       write(*,*)
       write(*,*) 'A = diag(d), d = '
       do j = 1, n
          write(*,*) d(j)
       end do
       write(*,*) 'b = '
       do j = 1, n
          write(*,*) b(j)
       end do
    end if

    checkA   = .true.          ! Set other parameters and solve.
    disable  = .false.
    itnlim   = n*2
    rtol     = 1.0e-12_dp
    maxxnorm = 1.0e+6_dp
    TranCond = 1.0e+7_dp
    Acondlim = 1.0e+15_dp

    if (debug) then
       write(*,*)
       write(*,*)  'n = ', n, ', precon = ', precon, ', shift = ', shift, ', pertM = ', pertM, &
                   ', sing = ', sing, ', consis = ', consis, ', nout = ', nout
       write(*,*)
       write(*,*)  'checkA = ', checkA, 'itnlim = ', itnlim, ', nout = ', nout,                &
                   ', maxxnorm = ', maxxnorm, ', TranCond = ', TranCond, ', Acondlim = ', Acondlim
    end if

    if (present(default)) then
        use_default = default
    else
        use_default = .false.
    end if

    if (use_default) then
       call MINRESQLP( n=n, Aprod=Aprod, b=b, shift=shift, nout=nout, x=x )
    else
       call MINRESQLP( n, Aprod, b, shift, Msolve, precon, disable,         &
                       nout, itnlim, rtol, maxxnorm, trancond, Acondlim,    &
                       x, istop, itn, rnorm, Arnorm, xnorm, Anorm, Acond )
    end if

    if (debug) then
       call Aprod (n,x,y)       ! y = A*x
       r1     = b - y + shift*x ! Final residual r1 = b - (A - shift*I)*x.
       r1norm = dnrm2(n,r1,1)
       write(nout,debugStr1) r1norm

       nprint = min(n,20)
       write(nout,*) ' '
       write(nout,*) 'Some of x'
       write(nout,debugStr2) (j, x(j), j=1,nprint)
    else
       nprint = min(n,5)
       write(nout,*) ' '
       write(nout,*) 'Some of b and x'
       do j=1,nprint
          write(nout,*) b(j), x(j)
       end do
    end if

    w      = x - xtrue            ! Print a clue about whether
    wnorm  = dnrm2(n,w,1)         ! the solution looks OK.
    xnormtrue  = dnrm2(n,xtrue,1)
    enorm  = wnorm/xnormtrue
    etol   = 1.0e-5_dp
    if (enorm <= etol) then
       write(nout, footerStr1) n, itn, enorm
    else
       write(nout, footerStr2) n, itn, enorm
    end if

    deallocate(d)                 ! Free work array

  end subroutine minresqlptest

  !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine minresqlptestMtxCDS(input_file, consis, nout, tol)

    character(80),  intent(in)           :: input_file
    logical,        intent(in)           :: consis
    integer(ip),    intent(in)           :: nout
    real(dp),       intent(in), optional :: tol

    !-------------------------------------------------------------------
    ! 29 Oct 2012: Use xcheck to check computed x from MINRESQLP.
    ! 02 Jan 2013: Print n in the " minresqlp  appears to be ..." message
    !              to help identify the problem.
    ! 21 Apr 2013: minresqlptestMtxCDS created from minresqlpmtxtest.
    !-------------------------------------------------------------------

    intrinsic      :: real, present

    integer(ip)    :: input_unit, nrow, ncol
    character(14)  :: id
    character(10)  :: rep
    character( 6)  :: type
    character(7)   :: field
    character(19)  :: symm

    real(dp), allocatable  :: b(:), x(:), r1(:), w(:)

    logical        :: checkA, disable, precon
    integer(ip)    :: n, j, itn, itnlim, istop, inform
    real(dp)       :: shift, Anorm, Acond, Arnorm, rnorm, rtol, xnorm
    real(dp)       :: maxxnorm, TranCond, Acondlim
    real(dp)       :: relTol, test1, test2

    character(len=*), parameter :: headerStr =             &
       "(// '---------------------------------------'"  // &
       "  / ' Test of MINRESQLP on an MM CDS matrix '"  // &
       "  / '---------------------------------------')"
    character(len=*), parameter :: footerStr1 = &
       "(/ '  minresqlp appears to be successful.  n =', i7, '  Itns =', i7," // &
       "'  test(r) =', 1p, e9.2, '  test(Ar) =', 1p, e9.2)"
    character(len=*), parameter :: footerStr2 = &
       "(/ '  minresqlp appears to have failed.    n =', i7, '  Itns =', i7," // &
       "'  test(r) =', 1p, e9.2, '  test(Ar) =', 1p, e9.2)"
!    character(len=*), parameter ::  debugStr1 = &
!       "(/ 'Final residual =', 1p, e8.1)"
!    character(len=*), parameter ::  debugStr2 = &
!       "(/ 'Solution  x', 4(i6, e14.6))"

    write(nout, headerStr)
    write(  * , headerStr)

    call ReadMtxSize( input_file, input_unit, &
                      id, type, rep, field, symm, nrow, ncol, nnz )
    rewind( input_unit )

    ! Now we know the size of the problem.
    ! We should allocate only the arrays that will be used by the MM routines.
    ! For simplicity we allocate them all.

    nnzmax = nnz
    allocate( indx(nnz), jndx(nnz), ival(nnz) )
    allocate( rval(nnz) )   ! CDS
    allocate( dval(nnz) )   ! CDS
    allocate( cval(nnz) )   ! CCH

    call ReadMtx( input_file, input_unit, &
                  id, rep, field, symm, nrow, ncol, nnz, &
                  indx, jndx, ival, rval, dval, cval )

    n = nrow

    relTol = 10_dp*eps
    if (present(tol)) then
       relTol = tol
    end if

    allocate( b(n) )
    allocate( x(n) )
    allocate( r1(n) )
    allocate( w(n) )

    do j = 1, n
       b(j) = one    ! real(j,dp)/real(n,dp)
       x(j) = real(j,dp)/n
    end do

    !write(nout,*) (j, b(j), j=1,5)  ! Print some of the solution

    !call AprodMtxCDS (n,b,x)   ! b = A*xtrue
    !write(nout,debugStr) (j, x(j), j=1,10)  ! Print some of the solution
    !call AprodMtxCDS (n,x,b)
    !write(nout,debugStr) (j, b(j), j=1,10)  ! Print some of the solution


    write(nout,*) 'consis     = ', consis
    write(nout,*) 'input_file = ', trim(input_file)
    write(nout,*) 'n = ', n, '  nnz = ', nnz

    checkA   = .true.          ! Set other parameters and solve.
    disable  = .false.
    precon   = .false.
    itnlim   = n*4
    rtol     = 1.0e-8_dp
    maxxnorm = 1.0e+8_dp
    TranCond = 1.0e+8_dp
    Acondlim = 1.0e+15_dp
    shift    = zero

    if (consis) then
       call AprodMtxCDS(n,x,b)   ! b = A*x
       write(nout,*) ' '
       write(nout,*) 'norm(b) =', dnrm2(n,b,1)
       write(nout,*) 'Some of the x defining b'
       do j = 1, min(n,5)
          write(nout,*) j, x(j)
       end do
    end if

    write(*,*) 'Some of b'
    do j = 1, min(n,5)
       write(nout,*) j, b(j)
    end do

    if (debug) then
       write(*,*)
       write(*,*)  'n = ', n, ', shift = ', shift,  ', consis = ', consis, ', nout = ', nout
       write(*,*)
       write(*,*)  'checkA = ', checkA, 'itnlim = ', itnlim, ', nout = ', nout,                &
                   ', maxxnorm = ', maxxnorm, ', TranCond = ', TranCond, ', Acondlim = ', Acondlim
    end if

    call MINRESQLP( n, AprodMtxCDS, b, shift, Msolve, precon, disable,   &
                    nout, itnlim, rtol, maxxnorm, trancond, Acondlim,    &
                    x, istop, itn, rnorm, Arnorm, xnorm, Anorm, Acond )

    call xcheck( n, AprodMtxCDS, b, shift, x, Anorm, tol, nout, &
                 test1, test2, inform )

    if (inform <= 2) then
       write(nout, footerStr1) n, itn, test1, test2
    else
       write(nout, footerStr2) n, itn, test1, test2
    end if

    write(nout,*) ' '
    write(nout,*) 'norm(x) =', dnrm2(n,x,1)
    write(nout,*) 'Some of the computed x'
    do j = 1, min(n,5)
       write(nout,*) x(j)
    end do

    deallocate( indx, jndx, ival, rval, dval, cval )
    deallocate( b, x, r1, w )

  end subroutine minresqlptestMtxCDS

  !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine minresqlptestMtxCPS(input_file, consis, nout, tol)

    character(80),  intent(in)           :: input_file
    logical,        intent(in)           :: consis
    integer(ip),    intent(in)           :: nout
    real(dp),       intent(in), optional :: tol

    !-------------------------------------------------------------------
    ! 29 Oct 2012: Use xcheck to check computed x from MINRESQLP.
    ! 02 Jan 2013: Print n in the " minresqlp  appears to be ..." message
    !              to help identify the problem.
    ! 21 Apr 2013: minresqlptestMtxCPS created from minresqlpmtxtest.
    !-------------------------------------------------------------------

    intrinsic      :: real, present

    integer(ip)    :: input_unit, nrow, ncol
    character(14)  :: id
    character(10)  :: rep
    character( 6)  :: type
    character(7)   :: field
    character(19)  :: symm

    real(dp), allocatable  :: b(:), x(:), r1(:), w(:)

    logical        :: checkA, disable, precon
    integer(ip)    :: n, j, itn, itnlim, istop, inform
    real(dp)       :: shift, Anorm, Acond, Arnorm, rnorm, rtol, xnorm
    real(dp)       :: maxxnorm, TranCond, Acondlim
    real(dp)       :: relTol, test1, test2

    character(len=*), parameter :: headerStr =             &
       "(// '---------------------------------------'"  // &
       "  / ' Test of MINRESQLP on an MM CPS matrix '"  // &
       "  / '---------------------------------------')"
    character(len=*), parameter :: footerStr1 = &
       "(/ '  minresqlp appears to be successful.  n =', i7, '  Itns =', i7," // &
       "'  test(r) =', 1p, e9.2, '  test(Ar) =', 1p, e9.2)"
    character(len=*), parameter :: footerStr2 = &
       "(/ '  minresqlp appears to have failed.    n =', i7, '  Itns =', i7," // &
       "'  test(r) =', 1p, e9.2, '  test(Ar) =', 1p, e9.2)"
!    character(len=*), parameter ::  debugStr1 = &
!       "(/ 'Final residual =', 1p, e8.1)"
!    character(len=*), parameter ::  debugStr2 = &
!       "(/ 'Solution  x', 4(i6, e14.6))"

    write(nout, headerStr)
    write(  * , headerStr)

    call ReadMtxSize( input_file, input_unit, &
                      id, type, rep, field, symm, nrow, ncol, nnz )
    rewind( input_unit )

    ! Now we know the size of the problem.
    ! We should allocate only the arrays that will be used by the MM routines.
    ! For simplicity we allocate them all.

    nnzmax = nnz
    allocate( indx(nnz), jndx(nnz), ival(nnz) )
    allocate( rval(nnz) )   ! CPS
    allocate( dval(nnz) )   ! CDS
    allocate( cval(nnz) )   ! CCH

    call ReadMtx( input_file, input_unit, &
                  id, rep, field, symm, nrow, ncol, nnz, &
                  indx, jndx, ival, rval, dval, cval )

    n = nrow

    relTol = 10_dp*eps
    if (present(tol)) then
       relTol = tol
    end if

    allocate( b(n) )
    allocate( x(n) )
    allocate( r1(n) )
    allocate( w(n) )

    do j = 1, n
       b(j) = one    ! real(j,dp)/real(n,dp)
       x(j) = real(j,dp)/n
    end do

    !call AprodMtxCPS (n,b,x)   ! b = A*xtrue
    !write(nout,debugStr) (j, x(j), j=1,10)  ! Print some of the solution
    !call AprodMtxCPS (n,x,b)
    !write(nout,debugStr) (j, b(j), j=1,10)  ! Print some of the rhs

    write(nout,*) 'consis     = ', consis
    write(nout,*) 'input_file = ', trim(input_file)
    write(nout,*) 'n = ', n, '  nnz = ', nnz

    checkA   = .true.          ! Set other parameters and solve.
    disable  = .false.
    precon   = .false.
    itnlim   = n*4
    rtol     = 1.0e-8_dp
    maxxnorm = 1.0e+8_dp
    TranCond = 1.0e+8_dp
    Acondlim = 1.0e+15_dp
    shift    = zero

    if (consis) then
       call AprodMtxCPS (n,x,b)   ! b = A*x
       write(nout,*) ' '
       write(nout,*) 'norm(b) =', dnrm2(n,b,1)
       write(nout,*) 'Some of the x defining b'
       do j = 1, min(n,5)
          write(nout,*) j, x(j)
       end do
    end if

    write(nout,*) 'Some of b'
    do j = 1, min(n,5)
       write(nout,*) j, b(j)
    end do

    if (debug) then
       write(*,*)
       write(*,*)  'n = ', n, ', shift = ', shift,  ', consis = ', consis, ', nout = ', nout
       write(*,*)
       write(*,*)  'checkA = ', checkA, 'itnlim = ', itnlim, ', nout = ', nout,                &
                   ', maxxnorm = ', maxxnorm, ', TranCond = ', TranCond, ', Acondlim = ', Acondlim
    end if

    call MINRESQLP( n, AprodMtxCPS, b, shift, Msolve, precon, disable,      &
                    nout, itnlim, rtol, maxxnorm, trancond, Acondlim,    &
                    x, istop, itn, rnorm, Arnorm, xnorm, Anorm, Acond )

    call xcheck( n, AprodMtxCPS, b, shift, x, Anorm, tol, nout, &
                 test1, test2, inform )

    if (inform <= 2) then
       write(nout, footerStr1) n, itn, test1, test2
    else
       write(nout, footerStr2) n, itn, test1, test2
    end if

    write(nout,*) ' '
    write(nout,*) 'norm(x) =', dnrm2(n,x,1)
    write(nout,*) 'Some of the computed x'
    do j = 1, min(n,5)
       write(nout,*) j, x(j)
    end do

    deallocate( indx, jndx, ival, rval, dval, cval )
    deallocate( b, x, r1, w )

  end subroutine minresqlptestMtxCPS

  !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine minresqlptestMtxCRS(input_file, consis, nout, tol)

    character(80),  intent(in)           :: input_file
    logical,        intent(in)           :: consis
    integer(ip),    intent(in)           :: nout
    real(dp),       intent(in), optional :: tol

    !-------------------------------------------------------------------
    ! 29 Oct 2012: Use xcheck to check computed x from MINRESQLP.
    ! 02 Jan 2013: Print n in the " minresqlp  appears to be ..." message
    !              to help identify the problem.
    !-------------------------------------------------------------------

    intrinsic      :: real, present

    integer(ip)    :: input_unit, nrow, ncol
    character(14)  :: id
    character(10)  :: rep
    character( 6)  :: type
    character(7)   :: field
    character(19)  :: symm

    real(dp), allocatable  :: b(:), x(:), r1(:), w(:)

    logical        :: checkA, disable, precon
    integer(ip)    :: n, j, itn, itnlim, istop, inform
    real(dp)       :: shift, Anorm, Acond, Arnorm, rnorm, rtol, xnorm
    real(dp)       :: maxxnorm, TranCond, Acondlim
    real(dp)       :: relTol, test1, test2

    character(len=*), parameter :: headerStr =             &
       "(// '---------------------------------------'"  // &
       "  / ' Test of MINRESQLP on an MM CRS matrix '"  // &
       "  / '---------------------------------------')"
    character(len=*), parameter :: footerStr1 = &
       "(/ '  minresqlp appears to be successful.  n =', i7, '  Itns =', i7," // &
       "'  test(r) =', 1p, e9.2, '  test(Ar) =', 1p, e9.2)"
    character(len=*), parameter :: footerStr2 = &
       "(/ '  minresqlp appears to have failed.    n =', i7, '  Itns =', i7," // &
       "'  test(r) =', 1p, e9.2, '  test(Ar) =', 1p, e9.2)"
!    character(len=*), parameter ::  debugStr1 = &
!       "(/ 'Final residual =', 1p, e8.1)"
!    character(len=*), parameter ::  debugStr2 = &
!       "(/ 'Solution  x', 4(i6, e14.6))"

    write(nout, headerStr)
    write(  * , headerStr)

    call ReadMtxSize( input_file, input_unit, &
                      id, type, rep, field, symm, nrow, ncol, nnz )
    rewind( input_unit )

    ! Now we know the size of the problem.
    ! We should allocate only the arrays that will be used by the MM routines.
    ! For simplicity we allocate them all.

    nnzmax = nnz
    allocate( indx(nnz), jndx(nnz), ival(nnz) )
    allocate( rval(nnz) )   ! CRS
    allocate( dval(nnz) )   ! CDS
    allocate( cval(nnz) )   ! CCH

    call ReadMtx( input_file, input_unit, &
                  id, rep, field, symm, nrow, ncol, nnz, &
                  indx, jndx, ival, rval, dval, cval )

    n = nrow

    relTol = 10_dp*eps
    if (present(tol)) then
       relTol = tol
    end if

    allocate( b(n) )
    allocate( x(n) )
    allocate( r1(n) )
    allocate( w(n) )

    do j = 1, n
       b(j) = one    ! real(j,dp)/real(n,dp)
       x(j) = real(j,dp)/n
    end do

    !write(nout,*) (j, b(j), j=1,5)  ! Print some of the solution

    !call AprodMtxCRS (n,b,x)   ! b = A*xtrue
    !write(nout,debugStr) (j, x(j), j=1,10)  ! Print some of the solution
    !call AprodMtxCRS (n,x,b)
    !write(nout,debugStr) (j, b(j), j=1,10)  ! Print some of the solution


    write(nout,*) 'consis     = ', consis
    write(nout,*) 'input_file = ', trim(input_file)
    write(nout,*) 'n = ', n, '  nnz = ', nnz

    checkA   = .true.          ! Set other parameters and solve.
    disable  = .false.
    precon   = .false.
    itnlim   = n*4
    rtol     = 1.0e-8_dp
    maxxnorm = 1.0e+8_dp
    TranCond = 1.0e+8_dp
    Acondlim = 1.0e+15_dp
    shift    = zero

    if (consis) then
       call AprodMtxCRS (n,x,b)   ! b = A*x
       write(nout,*) ' '
       write(nout,*) 'norm(b) =', dnrm2(n,b,1)
       write(nout,*) 'Some of the x defining b'
       do j = 1, min(n,5)
          write(nout,*) j, x(j)
       end do
    end if

    write(nout,*) 'Some of b'
    do j = 1, min(n,5)
       write(nout,*) j, b(j)
    end do

    if (debug) then
       write(*,*)
       write(*,*)  'n = ', n, ', shift = ', shift,  ', consis = ', consis, ', nout = ', nout
       write(*,*)
       write(*,*)  'checkA = ', checkA, 'itnlim = ', itnlim, ', nout = ', nout,                &
                   ', maxxnorm = ', maxxnorm, ', TranCond = ', TranCond, ', Acondlim = ', Acondlim
    end if

    call MINRESQLP( n, AprodMtxCRS, b, shift, Msolve, precon, disable,   &
                    nout, itnlim, rtol, maxxnorm, trancond, Acondlim,    &
                    x, istop, itn, rnorm, Arnorm, xnorm, Anorm, Acond )

    call xcheck( n, AprodMtxCRS, b, shift, x, Anorm, tol, nout, &
                 test1, test2, inform )

    if (inform <= 2) then
       write(nout, footerStr1) n, itn, test1, test2
    else
       write(nout, footerStr2) n, itn, test1, test2
    end if

    write(nout,*) ' '
    write(nout,*) 'norm(x) =', dnrm2(n,x,1)
    write(nout,*) 'Some of the computed x'
    do j = 1, min(n,5)
       write(nout,*) j, x(j)
    end do

    deallocate( indx, jndx, ival, rval, dval, cval )
    deallocate( b, x, r1, w )

  end subroutine minresqlptestMtxCRS

  !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine symorthotest( a, b, c_true, s_true, r_true, nout, tol )
    real(dp),    intent(in)           :: a, b, c_true, s_true, r_true
    real(dp),    intent(in), optional :: tol
    integer(ip), intent(in)           :: nout

    !-------------------------------------------------------------------
    ! 20 Aug 2012: First version of symorthotest.
    !-------------------------------------------------------------------

    intrinsic                   :: transpose, abs, sum

    real(dp)                    :: c, s, r, norm_diff
    real(dp)                    :: out(3), expected_out(3), relTol
    character(len=*), parameter :: footerStr1 = &
       "(/ '  symortho  appears to be successful.  Relative error in [c,s,r] =', 1p, e8.1)"
    character(len=*), parameter :: footerStr2 = &
       "(/ '  symortho  appears to have failed.    Relative error in [c,s,r] =', 1p, e8.1)"

    call symortho( a, b, c, s, r )

    out          = (/    c  , s     , r      /)
    expected_out = (/ c_true, s_true, r_true /)
    norm_diff    = dnrm2( 3, out - expected_out, 1 ) / dnrm2( 3, expected_out, 1 )

    if (.not. present(tol)) then
       relTol = eps
    else
       relTol = tol
    end if

    write(nout,*)  ' '
    write(nout,*)  '-----------------------------------------------------'
    write(nout,*)  'Test of  SYMORTHO.'
    write(nout,*)  '-----------------------------------------------------'
    write(nout,*)  'a = ',  a, '  b = ', b,  '  relTol = ', relTol
    write(nout,*)  ' '
    write(nout,*)  '[c,s,r]      = ',  out
    write(nout,*)  'true [c,s,r] = ',  expected_out

    if (norm_diff < relTol) then
       write(nout,footerStr1) norm_diff
    else
       write(nout,footerStr2) norm_diff
    end if

  end subroutine symorthotest

end module minresqlpTestModule
