!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! File minresqlpCheckModule.f90
!
!    xcheck
!
! xcheck tests if a given x seems to be a solution of symmetric system
! Ax = b or Ax ~= b.
!
! 29 Oct 2012: minresqlpCheckModule.f90 derived from lsmrCheckModule.f90.
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

module minresqlpCheckModule

  use  minresqlpDataModule,    only : dp, ip, zero
  use  minresqlpBlasModule,    only : ddot, dnrm2
  implicit none
  private
  public   :: xcheck

contains

  !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine xcheck( n, Aprod, b, shift, x, Anorm, tol, nout, &
                     test1, test2, inform )

    integer(ip), intent(in)    :: n        ! No. of rows and cols of A
    integer(ip), intent(in)    :: nout     ! Output file number
    integer(ip), intent(out)   :: inform   ! = 0 if b = 0 and x = 0.
                                           ! = 1 or 2 if x seems to
                                           !   solve systems 1 or 2 below;
                                           ! = 4 otherwise.
    real(dp),    intent(in)    :: Anorm    ! An estimate of norm(A - shift*I)
                                           ! provided by MINRESQLP.
    real(dp),    intent(in)    :: tol      ! tolerance for judging residuals.
                                           ! Typically the tol used for computing x.
    real(dp),    intent(in)    :: shift    ! Often zero.
    real(dp),    intent(in)    :: b(n)     ! The right-hand side of (A - shift*I)x ~= b.
    real(dp),    intent(in)    :: x(n)     ! The given solution estimate.
    real(dp),    intent(out)   :: test1    ! Should be small for consistent systems.
    real(dp),    intent(out)   :: test2    ! Should be small for LS systems.

    interface
       subroutine Aprod(n,x,y)          ! y := A*x
         use minresqlpDataModule, only : dp, ip
         integer(ip), intent(in)    :: n
         real(dp),    intent(in)    :: x(n)
         real(dp),    intent(out)   :: y(n)
       end subroutine Aprod
    end interface

    !-------------------------------------------------------------------
    ! One-liner: xcheck tests if x solves symmetric (A-shift*I)x=b.
    !
    ! Purpose:   xcheck computes residuals and norms associated with
    ! the vector x and the problem solved by MINRESQLP.
    ! It determines whether x seems to be a solution to either of the
    ! systems:  1. (A - shift*I)x = b
    !           2. min ||(A - shift*I)x - b||
    !
    ! History:
    ! 29 Oct 2012: xcheck derived from xcheck in lsmrCheckModule.f90.
    ! 02 Jan 2013: Print Anorm, xnorm, bnorm.
    !-------------------------------------------------------------------

    intrinsic           :: epsilon, max

    ! Local variables and arrays
    real(dp)            :: r(n), v(n)
    real(dp)            :: bnorm, eps, rnorm, sigma, tol2, xnorm

    eps    = epsilon(eps)
    tol2   = max( tol, eps )

    call Aprod(n,x,r)        ! r = Ax
    r      = b - r + shift*x ! r = b - (A - shift*I)x = b - Ax + shift*x
    call Aprod(n,r,v)        ! v = Ar
    v      = v - shift*r     ! v = (A - shift*I)r = Ar - shift*r

    bnorm  = dnrm2 (n,b,1)   ! Compute the norms of b, x, r, v.
    xnorm  = dnrm2 (n,x,1)
    rnorm  = dnrm2 (n,r,1)
    sigma  = dnrm2 (n,v,1)

    if (nout > 0) write(nout,2200) shift, Anorm, xnorm, bnorm, rnorm, sigma

    !-------------------------------------------------------------------
    ! See if x seems to solve (A - shift*I)x = b  or  min ||(A - shift*I)x - b||.
    !-------------------------------------------------------------------
    if (bnorm == zero  .and.  xnorm == zero) then
       inform = 0
       test1  = zero
       test2  = zero
    else
       inform = 4
       test1  = rnorm / (Anorm*xnorm + bnorm)
       test2  = zero
       if (rnorm >  zero) test2  = sigma / (Anorm*rnorm)

       if (test2 <= tol2) inform = 2
       if (test1 <= tol2) inform = 1
    end if

    if (nout > 0) write(nout,3000) inform, tol2, test1, test2
    return

 2200 format(1p                    &
      // ' Enter xcheck.  Does x solve Ax = b?  where A is really (A - shift*I)' &
      /  '    shift     =', e10.3  &
      /  '    norm(A)   =', e10.3  &
      /  '    norm(x)   =', e10.3  &
      /  '    norm(b)   =', e10.3  &
      /  '    norm(r)   =', e15.8  &
      /  '    norm(Ar)  =', e10.3)
 3000 format(1p                    &
      /  '    inform    =', i2     &
      /  '    tol       =', e10.3  &
      /  '    test1     =', e10.3, ' (Ax = b)            rnorm/(Anorm*xnorm + bnorm)' &
      /  '    test2     =', e10.3, ' (least-squares)    Arnorm/(Anorm*rnorm)')

  end subroutine xcheck

end module minresqlpCheckModule
