The datasets in this directory are in Matrix Market CRS format
(coordinate real symmetric).

15 Sep 2012: First version of readme.txt.
             11 singular examples included in DataMtx/CRS directory.

The following examples are all singular.  They are from the SJSU
Singular Matrix Database maintained by Leslie Foster, Mathematics Dept,
San Jose State University:
   http://www.math.sjsu.edu/singular/matrices/

aft01.mtx
bcsstm13.mtx
bloweybq.mtx
c-30.mtx
c-41.mtx
exdata_1.mtx
eurqsa.mtx
laser.mtx
saylr3.mtx
shaw_100.mtx
t2dal.mtx

