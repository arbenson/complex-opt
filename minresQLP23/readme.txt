-----------------------------------------------------------------------------
readme.txt for the Fortran 90 implementation of MINRES-QLP
(real and complex versions).
Sou-Cheng Choi   <sctchoi@uchicago.edu>
Michael Saunders <saunders@stanford.edu>

16 Sep 2012: Version 20 of the f90 implementation.
             mm_ioModule.f90 is derived from John Burkhardt's mm_io.f90
                http://people.sc.fsu.edu/~jburkardt/f_src/mm_io/
             to permit input of sparse matrix test data in
             Matrix Market (MM) format.

31 Oct 2012: Version 21.
             Includes subroutines xcheck and zxcheck in
                minresqlpCheckModule.f90
               zminresqlpCheckModule.f90
             to test if a computed x satisfies Ax=b or AAx=Ab (Ar=0).

05 Jan 2013: Version 22.
             Numerous minor edits.  emacs whitespace-cleanup helps.
             More MM examples added to DataMtx/CRS and DataMtx/CCH
             (except there are almost no "coordinate complex hermitian"
             examples in the UFL or SJSU collections).

             The MM input routines (minresqlpReadMtxModule.f90)
             determine the matrix size and allocate storage at run-time.

             Currently just 1 test fails: DataMtx/CRS/mhd3200b.mtx.
             xcheck reveals that norm(r) for the computed x
             differs significantly from minresqlp's estimate.

24 Apr 2013: Version 23.
             istop = 12 now means xnorm just exceeded maxxnorm.
28 Jun 2013: likeLS introduced to terminate with big xnorm
             only if the problem seems to be singular and inconsistent.
             All test problems solve smoothly now.

-----------------------------------------------------------------------------

To test the real(8) implementation (minresqlp) on Unix or Linux,
   make
   ./minresqlptest
   grep minresqlp MINRESQLP.txt

Some of the tests use data in Matrix Market (MM) format
downloaded from Tim Davis's UFL Sparse Matrix Collection
and Les Foster's SJSU Singular Sparse Matrix Collection
   http://www.cise.ufl.edu/research/sparse/matrices/
   http://www.math.sjsu.edu/singular/matrices/
They are stored in DataMtx/CRS/ (coordinate real symmetric).
Beware that matrix values are stored in real(4) rval(*).
If line 1 of the *.mtx data files were changed from
   %%MatrixMarket matrix coordinate real symmetric
to %%MatrixMarket matrix coordinate double symmetric,
the matrix values would be stored in real(8) dval(*).
(Subroutine minresqlpmtxtest in minresTestModule.f90
should then have AprodMtxCRS changed to AprdMtxCDS everywhere.)

The test matrices are used twice with variable "consis" true or false.
If consis=T, the rhs is computed as b = A*xtrue
and the computed solution should have norm(r) small.
If consis=F, the rhs is alteredin b(n-1:n).
If A is singular, norm(r) will not be small, but norm(A*r) will be.


To test the complex(8) implementation (zminreslqlp) on Unix or Linux:
   make -f zMakefile
   ./zminresqlptest
   grep zminresqlp zMINRESQLP.txt

Again, some of the tests use data in Matrix Market (MM) format
downloaded from the UFL and SJSU Sparse Matrix Collections:
   http://www.cise.ufl.edu/research/sparse/matrices/
   http://www.math.sjsu.edu/singular/matrices/
They are stored in DataMtx/CCH/ (coordinate complex hermitian).
Matrix values are stored in complex(8) cval(*).
Currently 2 of the test cases are singular.
