!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! File zlsmrDataModule.f90
!
! Extends lsmrDataModule.f90 for use with complex numbers
! 29 Jun 2013: File created
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

module zlsmrDataModule

  use    lsmrDataModule, only  :  dp, sp, ip, zero, one
  implicit none

  intrinsic                      ::      cmplx
  complex(dp), parameter, public :: zzero = cmplx(zero,zero,dp), zone = cmplx(one,zero,dp)

end module zlsmrDataModule
